#include "stdafx.h"
#include "CppUnitTest.h"
#include "../memory_alloc_mj/memory_mng_stack.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace testmemory_alloc_mj
{
	class CrapTestClass {

	public:
		CrapTestClass() {};
		CrapTestClass(size_t a, size_t b) : a(a), b(b) {};
		size_t a;
		size_t b;
	};

	TEST_CLASS(UnitTest1)
	{
	public:
		const size_t _crap_size = sizeof(CrapTestClass);
		
		TEST_METHOD(mng_simple_alloc)
		{
			auto mng = memory_mng_stack(16);
			auto res = mng.allocate_new<CrapTestClass>();
			Assert::IsTrue(nullptr != res);
			Assert::AreEqual(size_t(1), mng.get_allocated_count());
			Assert::AreEqual(_crap_size, mng.get_allocated_mem());
		}

		TEST_METHOD(mng_simple_de_alloc)
		{
			auto mng = memory_mng_stack(16);
			auto res = mng.allocate_new<CrapTestClass>();
			mng.de_allocate<CrapTestClass>(*res);
			Assert::IsTrue(nullptr != res);
			Assert::AreEqual(size_t(0), mng.get_allocated_count());
			Assert::AreEqual(size_t(0), mng.get_allocated_mem());
		}

		TEST_METHOD(mng_too_much)
		{
			auto mng = memory_mng_stack(16);
			auto res = mng.allocate_new<CrapTestClass>();
			res = mng.allocate_new<CrapTestClass>();
			res = mng.allocate_new<CrapTestClass>();

			Assert::IsTrue(nullptr == res);
		}

		TEST_METHOD(mng_too_much_de_allocate)
		{
			auto mng = memory_mng_stack(16);
			auto tmp = mng.allocate_new<CrapTestClass>();
			mng.de_allocate<CrapTestClass>(*tmp);
			auto res = mng.de_allocate<CrapTestClass>(*tmp);

			Assert::IsTrue(nullptr == res);
		}
	};
}