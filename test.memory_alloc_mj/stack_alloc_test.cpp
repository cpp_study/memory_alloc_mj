#include "stdafx.h"
#include "CppUnitTest.h"
#include "../memory_alloc_mj/stack_alloc.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace testmemory_alloc_mj
{		
	class CrapTestClass {

	public:
		CrapTestClass(size_t a, size_t b) : a(a), b(b) {};
		size_t a;
		size_t b;
	};

	TEST_CLASS(UnitTest1)
	{
	public:
		const size_t _crap_size = sizeof(CrapTestClass);
		
		TEST_METHOD(simple_alloc)
		{
			void* mem = malloc(16);
			auto allocator = new stack_alloc(mem, 16);
			auto res = allocator->allocate(_crap_size);

			Assert::IsTrue(nullptr != res);
			Assert::AreEqual(size_t(1), allocator->get_allocated_count());
			Assert::AreEqual(_crap_size, allocator->get_allocated_memory());
		}

		TEST_METHOD(too_much)
		{
			void* mem = malloc(12);
			auto allocator = new stack_alloc(mem, 12);
			allocator->allocate(_crap_size);
			auto res = allocator->allocate(_crap_size);
			
			Assert::IsTrue(nullptr == res);
		}

		TEST_METHOD(multiple_alloc)
		{
			void* mem = malloc(24);
			auto allocator = new stack_alloc(mem, 24);
			auto res = allocator->allocate(_crap_size);
			res = allocator->allocate(_crap_size);
			res = allocator->allocate(_crap_size);

			Assert::IsTrue(nullptr != res);
			Assert::AreEqual(size_t(3), allocator->get_allocated_count());
			Assert::AreEqual(_crap_size * 3, allocator->get_allocated_memory());
		}

		TEST_METHOD(multiple_alloc_too_much)
		{
			void* mem = malloc(24);
			auto allocator = new stack_alloc(mem, 24);
			allocator->allocate(_crap_size);
			allocator->allocate(_crap_size);
			allocator->allocate(_crap_size);
			auto res = allocator->allocate(_crap_size);
			
			Assert::IsTrue(nullptr == res);
		}

		TEST_METHOD(simple_de_alloc)
		{
			void* mem = malloc(16);
			auto allocator = new stack_alloc(mem, 16);
			auto res = allocator->allocate(_crap_size);
			allocator->deallocate(_crap_size);
			Assert::IsTrue(nullptr != res);
			Assert::AreEqual(size_t(0), allocator->get_allocated_count());
			Assert::AreEqual(size_t(0), allocator->get_allocated_memory());
		}

		TEST_METHOD(too_much_de_alloc)
		{
			void* mem = malloc(16);
			auto allocator = new stack_alloc(mem, 16);
			new(allocator->allocate(_crap_size)) CrapTestClass{ 2, 5 };
			allocator->deallocate(_crap_size);
			auto res = allocator->deallocate(_crap_size);

			Assert::IsTrue(nullptr == res);
		}

		TEST_METHOD(clear)
		{
			void* mem = malloc(24);
			auto allocator = new stack_alloc(mem, 24);
			allocator->allocate(_crap_size);
			allocator->allocate(_crap_size);
			allocator->allocate(_crap_size);
			auto res = allocator->clear();

			Assert::IsTrue(mem  == res);
			Assert::AreEqual(size_t(0), allocator->get_allocated_count());
			Assert::AreEqual(size_t(0), allocator->get_allocated_memory());
		}
	};
}