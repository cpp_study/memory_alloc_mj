#pragma once
#include "stdafx.h"
#include "bit_masks.h"
#include <vector>

size_t as_binary(char data, std::vector<size_t>& output) {
	if (output.size() != size_t(8)) {
		return -1;
	}

	if ((data & BIT_0) == BIT_0) {
		output[7] = 1;
	}

	if ((data & BIT_1) == BIT_1) {
		output[6] = 1;
	}

	if ((data & BIT_2) == BIT_2) {
		output[5] = 1;
	}


	if ((data & BIT_3) == BIT_3) {
		output[4] = 1;
	}

	if ((data & BIT_4) == BIT_4) {
		output[3] = 1;
	}

	if ((data & BIT_5) == BIT_5) {
		output[2] = 1;
	}

	if ((data & BIT_6) == BIT_6) {
		output[1] = 1;
	}

	if ((data & BIT_7) == BIT_7) {
		output[0] = 1;
	}

	return 0;
}

