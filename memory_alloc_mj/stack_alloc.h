#pragma once

// 20180114_mj: simple stack allocator. allocator only concerns itself with adding/removing memory
// it should "live" within a memory manager that will mainatin safe checks etc. 
// or just go for it, you rebel
class stack_alloc {
public:
	stack_alloc(void* start, size_t size);
	~stack_alloc();
	void* allocate(size_t size);
	void* deallocate(size_t size);
	void* clear();
	size_t get_allocated_memory();
	size_t get_allocated_count();

private:
	void* _head = nullptr;
	size_t _allocated_mem;
	size_t _allocated_count;
	size_t _top_limit;
};
