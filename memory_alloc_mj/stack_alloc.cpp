#pragma once
#include "stdafx.h"
#include "stack_alloc.h"

stack_alloc::stack_alloc(void* start, size_t size) {
	_head = start;
	_top_limit = size;
	_allocated_mem = 0;
	_allocated_count = 0;
}

stack_alloc::~stack_alloc() {
	delete _head;
};

void* stack_alloc::allocate(size_t size) {
	_allocated_mem += size;

	if (_allocated_mem > _top_limit) {
		_allocated_mem -= size;

		return nullptr;
	}

	auto res = _head;
	_head = static_cast<char*>(_head) + size;
	_allocated_count++;
	
	return res;
}

// 20180115_mj: this feel stupid. you can just go on and remove a random 
// chunk of memory. needs some fancy maintaing or just "load once clear once" approach
void* stack_alloc::deallocate(size_t size) {
	if (_allocated_mem < size)	{
		return nullptr;
	}

	_head = static_cast<char*>(_head) - size;
	_allocated_mem -= size;
	_allocated_count--;

	return _head;
}

//20180115_mj: this makes more sense. at least it announces what's going to happen.
void* stack_alloc::clear() {
	_head = static_cast<char*>(_head) - _allocated_mem;
	_allocated_mem -= _allocated_mem;
	_allocated_count -= _allocated_count;

	return _head;
}

size_t stack_alloc::get_allocated_memory() {
	return _allocated_mem;
}

size_t stack_alloc::get_allocated_count() {
	return _allocated_count;
}
