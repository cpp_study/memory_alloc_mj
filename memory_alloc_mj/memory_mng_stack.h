#pragma once
#include "stack_alloc.h"

//20180114_mj: wrapper on the top of allocator - mainly used to initialize memory and provide functions
// to create new objects
class memory_mng_stack
{
public:
	memory_mng_stack(size_t size);
	~memory_mng_stack();
	size_t get_size();
	size_t get_allocated_mem();
	size_t get_allocated_count();
	
	template <class T>
	T* allocate_new() {
		auto address = _alloc->allocate(sizeof(T));
		if (address == nullptr) {
			return nullptr;
		}

		return new(address) T;
	}

	template <class T>
	void* de_allocate(T& target) {
		auto res = _alloc->deallocate(sizeof(T));
		
		if (res == nullptr) {
			return nullptr;
		}

		target.~T();

		return res;
	}

private:
	void* _private_heap = nullptr;
	size_t _size;
	stack_alloc* _alloc;
};
