#pragma once
#include "stdafx.h"
#include "stdlib.h"
#include "memory_mng_stack.h"

memory_mng_stack::memory_mng_stack(size_t size) {
	_size = size;
	_private_heap = malloc(_size);
	_alloc = new stack_alloc(_private_heap, size);
}

memory_mng_stack::~memory_mng_stack()
{
	delete _private_heap;
}

size_t memory_mng_stack::get_size() {
	return _size;
}

size_t memory_mng_stack::get_allocated_mem() {
	return _alloc->get_allocated_memory();
}

size_t memory_mng_stack::get_allocated_count() {
	return _alloc->get_allocated_count();
}